package exceptions;

import model.Lexeme;

public class SyntaxAnalyzerException extends RuntimeException {

    private Lexeme lexeme;

    public SyntaxAnalyzerException(String message, Lexeme lexeme) {
        this(message + "\n" + lexeme.toString() +
                "\n\tстрока: " + lexeme.getLine() +
                "\n\tпозиция: " + lexeme.getPosition()
        );
    }

    public Lexeme getLexeme() {
        return lexeme;
    }

    public SyntaxAnalyzerException() {
        super();
    }

    public SyntaxAnalyzerException(String message) {
        super(message);
    }

    public SyntaxAnalyzerException(String message, Throwable cause) {
        super(message, cause);
    }

    public SyntaxAnalyzerException(Throwable cause) {
        super(cause);
    }

    protected SyntaxAnalyzerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
