package exceptions;

import analyze.Scanner;

public class NotReturnOperatorException extends SemanticAnalyzerException {

    public static final String MESSAGE = "Отсутствует оператор return в функции %s";

    public NotReturnOperatorException(String identifier, Scanner scanner) {
        super(String.format(MESSAGE, identifier), scanner);
    }
}
