package exceptions;

import analyze.Scanner;
import enums.DataType;

public class TypeCastingException extends SemanticAnalyzerException {

    public static final String MESSAGE = "Невозможно привести тип %s к типу %s";

    public TypeCastingException(DataType toType, DataType fromType, Scanner scanner) {
        super(String.format(MESSAGE, fromType, toType), scanner);
    }
}
