package exceptions;

import analyze.Scanner;

public class NotFunctionException extends SemanticAnalyzerException {

    public static final String MESSAGE = "%s не является функцией";

    public NotFunctionException(String identifier, Scanner scanner) {
        super(String.format(MESSAGE, identifier), scanner);
    }
}
