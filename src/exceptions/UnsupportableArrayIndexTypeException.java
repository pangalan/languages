package exceptions;

import analyze.Scanner;
import enums.DataType;

public class UnsupportableArrayIndexTypeException extends SemanticAnalyzerException {

    public static final String MESSAGE = "Индекс массива не может быть типа %s";

    public UnsupportableArrayIndexTypeException(DataType dataType, Scanner scanner) {
        super(String.format(MESSAGE, dataType), scanner);
    }
}
