package exceptions;

import analyze.Scanner;

public class SemanticAnalyzerException extends RuntimeException {

    private static final String MESSAGE = "Ошибка на строке %d на позиции %d:\n\t%s";

    public SemanticAnalyzerException(String message, Scanner scanner) {
        super(String.format(MESSAGE, scanner.getCurrentState().getLine() + 1,
                            scanner.getCurrentState().getPosition() + 1,
                            message));
    }
}
