package exceptions;

import analyze.Scanner;

public class NotArrayException extends SemanticAnalyzerException {

    public static final String MESSAGE = "%s не является массивом";

    public NotArrayException(String identifier, Scanner scanner) {
        super(String.format(MESSAGE, identifier), scanner);
    }
}
