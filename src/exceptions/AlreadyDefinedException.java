package exceptions;

import analyze.Scanner;

public class AlreadyDefinedException extends SemanticAnalyzerException {

    public static final String MESSAGE = "Переменная с таким именем \"%s\" уже объявлена";

    public AlreadyDefinedException(String identifier, Scanner scanner) {
        super(String.format(MESSAGE, identifier), scanner);
    }
}
