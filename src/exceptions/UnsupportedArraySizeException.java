package exceptions;

import analyze.Scanner;

public class UnsupportedArraySizeException extends SemanticAnalyzerException {

    public static final String MESSAGE = "Количество размерностей массива превышает допустимую";

    public UnsupportedArraySizeException(Scanner scanner) {
        super(MESSAGE, scanner);
    }
}
