package exceptions;

import analyze.Scanner;

public class NotVariableException extends SemanticAnalyzerException {

    public static final String MESSAGE = "%s не является переменной";

    public NotVariableException(String identifier, Scanner scanner) {
        super(String.format(MESSAGE, identifier), scanner);
    }
}
