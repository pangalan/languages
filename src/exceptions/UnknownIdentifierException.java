package exceptions;

import analyze.Scanner;

public class UnknownIdentifierException extends SemanticAnalyzerException {

    public static final String MESSAGE = "Незвестный идентификтор %s";

    public UnknownIdentifierException(String identifier, Scanner scanner) {
        super(String.format(MESSAGE, identifier), scanner);
    }
}
