import analyze.Scanner;
import analyze.SyntaxAnalyzer;
import exceptions.SemanticAnalyzerException;
import exceptions.SyntaxAnalyzerException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(new File("input.cpp")));
        List<String> lines = new ArrayList<>();
        for (String str = in.readLine(); str != null; str = in.readLine()) {
            lines.add(str + "\n");
        }
        Scanner scanner = new Scanner(lines);
        SyntaxAnalyzer syntaxAnalyzer = new SyntaxAnalyzer(scanner);
        try {
            syntaxAnalyzer.program();
            System.out.println("Входной файл проанализирован без ошибок");
        } catch (SyntaxAnalyzerException | SemanticAnalyzerException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            syntaxAnalyzer.printTree();
        }
    }

}
