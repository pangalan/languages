package enums;

public enum Type {

    KeyDo("do"),
    KeyWhile("while"),
    KeyConst("const"),
    KeyFloat("float"),
    KeyChar("char"),
    KeyReturn("return"),
    TIdent,
    TConstString,
    TConstChar,
    TConst10,
    TConst16,
    TComma(","),
    TSemicolon(";"),
    TOpenedParen("("),
    TClosedParen(")"),
    TOpenedBrace("{"),
    TClosedBrace("}"),
    TOpenedBracket("["),
    TClosedBracket("]"),
    TLessThen("<"),
    TLessThenOrEq("<="),
    TGreaterThen(">"),
    TGreaterThenOrEq(">="),
    TEq("=="),
    TIneq("!="),
    TPlus("+"),
    TDoublePlus("++"),
    TMinus("-"),
    TDoubleMinus("--"),
    TMultiple("*"),
    TDivision("/"),
    TMod("%"),
    TAssign("="),
    TEnd("end"),
    TError;

    private final String displayName;

    Type() { displayName = null; }
    Type(String s) { displayName = s; }

    public String getDisplayName() {
        return displayName;
    }
}
