package enums;

public enum DataType {

    tChar("char"),
    tFloat("float"),
    tBlock("block"),
    tUnknown("unknown");

    private String value;

    DataType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


    @Override
    public String toString() {
        return value;
    }
}
