package analyze;

import analyze.exception.AnalyzeExceptionBuilder;
import analyze.exception.PrintingAnalyzeExcpetionBuilder;
import enums.DataType;
import exceptions.SyntaxAnalyzerException;
import model.Lexeme;
import model.State;

import static enums.Type.*;

public class SyntaxAnalyzer {

    private final Scanner scanner;
    private final SemanticAnalyzer semanticAnalyzer;
    private final AnalyzeExceptionBuilder analyzeExceptionBuilder;

    public SyntaxAnalyzer(Scanner scanner) {
        this.scanner = scanner;
        analyzeExceptionBuilder = new PrintingAnalyzeExcpetionBuilder(scanner);
        semanticAnalyzer = new SemanticAnalyzer(scanner, analyzeExceptionBuilder);
    }

    /**
     * S -> S W | e
     */
    public void program() {
        while (scanner.getNextAndRestoreState().getType() != TEnd) {
            description();
        }
    }

    public void printTree() {
        System.out.println(semanticAnalyzer.treeToString());
    }

    /**
     * W -> D | F
     * T -> float | char
     */
    private void description() {
        State currentState = scanner.getCurrentState();
        Lexeme lexeme = scanner.getNext();
        resolveType(lexeme);
        lexeme = scanner.getNext();
        lexemeIsIdent(lexeme);
        lexeme = scanner.getNext();
        scanner.restoreState(currentState);
        if (lexeme.getType() == TOpenedParen) {
            function();
        } else {
            data();
        }
    }

    /**
     * F -> T i () B
     * T -> float | char
     */
    private void function() {
        Lexeme lexeme = scanner.getNext();
        DataType type = resolveType(lexeme);
        lexeme = scanner.getNext();
        lexemeIsIdent(lexeme);
        Lexeme function = lexeme;
        semanticAnalyzer.addFunction(type, lexeme);
        lexeme = scanner.getNext();
        if (lexeme.getType() != TOpenedParen)
            symbolExpected('(', lexeme);
        lexeme = scanner.getNext();
        if (lexeme.getType() != TClosedParen)
            symbolExpected(')', lexeme);
        block();
        if (!semanticAnalyzer.checkReturn()) {
            analyzeExceptionBuilder.notReturnOperator(function.getValue());
        }
        semanticAnalyzer.goToParent();
    }

    /**
     * D -> T Vs ;
     * Vs -> Vs, V | V
     * V -> i | i = E | A
     * E -> O1
     * T -> float | char
     * A -> i [z10] | i [z10] = Ct | i [ z10] = s
     */
    private void data() {
        Lexeme lexeme = scanner.getNext();
        DataType type = resolveType(lexeme);
        do {
            lexeme = scanner.getNext();
            lexemeIsIdent(lexeme);
            semanticAnalyzer.addVariable(type, lexeme);
            lexeme = scanner.getNext();
            switch (lexeme.getType()) {
                case TAssign: {
                    DataType expressionType = o1();
                    if (!semanticAnalyzer.checkCompatibility(type, expressionType)) {
                        analyzeExceptionBuilder.typeCastingException(type, expressionType);
                    }
                    lexeme = scanner.getNext();
                    break;
                }
                case TOpenedBracket: {
                    lexeme = scanner.getNext();
                    if (lexeme.getType() != TConst10)
                        throw new SyntaxAnalyzerException("Ожидалась целая десятичная константа", lexeme);
                    semanticAnalyzer.addArrayLevel(Integer.parseInt(lexeme.getValue()));
                    lexeme = scanner.getNext();
                    if (lexeme.getType() != TClosedBracket)
                        symbolExpected(']', lexeme);
                    lexeme = scanner.getNext();
                    if (lexeme.getType() == TAssign) {
                        State currentState = scanner.getCurrentState();
                        lexeme = scanner.getNext();
                        if (lexeme.getType() != TConstString) {
                            scanner.restoreState(currentState);
                            constantList(type);
                        } else {
                            if (!semanticAnalyzer.checkCompatibility(type, DataType.tChar)) {
                                analyzeExceptionBuilder.typeCastingException(type, DataType.tChar);
                            }
                        }
                        lexeme = scanner.getNext();
                    }
                }
            }
        } while (lexeme.getType() == TComma);
        if (lexeme.getType() != TSemicolon)
            symbolExpected(';', lexeme);
    }

    /**
     * Ct -> { Cs }
     * Cs -> Cs, C | C
     * C -> s | c | z10 | z16
     */
    private void constantList(DataType type) {
        Lexeme lexeme = scanner.getNext();
        if (lexeme.getType() != TOpenedBrace)
            symbolExpected('{', lexeme);
        do {
            lexeme = scanner.getNext();
            DataType constantType = null;
            switch (lexeme.getType()) {
                case TConstChar:
                    constantType = DataType.tChar;
                    break;
                case TConst10:
                    constantType = DataType.tFloat;
                    break;
                case TConst16:
                    constantType = DataType.tFloat;
                    break;
                default:
                    throw new SyntaxAnalyzerException("Ожидалось одна из констант: символ, " +
                                                              "целое число в 10 с/с, целое число в 16 с/с", lexeme);
            }
            if (!semanticAnalyzer.checkCompatibility(type, constantType)) {
                analyzeExceptionBuilder.typeCastingException(type, constantType);
            }
            lexeme = scanner.getNext();
        } while (lexeme.getType() == TComma);
        //lexeme = scanner.getNext();
        if (lexeme.getType() != TClosedBrace)
            symbolExpected('}', lexeme);
    }

    /**
     * B -> { Ms }
     */
    private void block() {
        semanticAnalyzer.addBlock();
        Lexeme lexeme = scanner.getNext();
        if (lexeme.getType() != TOpenedBrace)
            symbolExpected('{', lexeme);
        operatorList();
        lexeme = scanner.getNext();
        if (lexeme.getType() != TClosedBrace)
            symbolExpected('}', lexeme);
        semanticAnalyzer.goToParent();
    }

    /**
     * Ms -> Ms M | Ms D | Ms B | e
     */
    private void operatorList() {
        while (true) {
            Lexeme lexeme = scanner.getNextAndRestoreState();
            if (lexeme.getType() == TIdent ||
                    lexeme.getType() == TSemicolon ||
                    lexeme.getType() == KeyDo ||
                    lexeme.getType() == KeyReturn ||
                    lexeme.getType() == TOpenedBrace) {
                operator();
                continue;
            }
            if (lexeme.getType() == KeyFloat || lexeme.getType() == KeyChar) {
                data();
                continue;
            }
            break;
        }
    }

    /**
     * M -> Y | ; | Dw | R | Fc
     */
    private void operator() {
        State currentState = scanner.getCurrentState();
        Lexeme lexeme = scanner.getNext();
        switch (lexeme.getType()) {
            case TIdent: {
                lexeme = scanner.getNext();
                if (lexeme.getType() == TOpenedParen) {
                    scanner.restoreState(currentState);
                    functionCall();
                    break;
                }
                if (lexeme.getType() == TAssign || lexeme.getType() == TOpenedBracket) {
                    scanner.restoreState(currentState);
                    assignOperator();
                    break;
                }
                throw new SyntaxAnalyzerException("Ожидался один из символов: '(', '=', '['", lexeme);
            }
            case TSemicolon:
                break;
            case KeyDo: {
                scanner.restoreState(currentState);
                doWhileOperator();
                break;
            }
            case KeyReturn: {
                scanner.restoreState(currentState);
                returnOperator();
                break;
            }
            case TOpenedBrace: {
                scanner.restoreState(currentState);
                block();
                break;
            }
            default:
                throw new SyntaxAnalyzerException(
                        "Ожидалось одно из выражений: идентификатор, ';', 'do', 'return", lexeme);
        }
    }

    /**
     * Fc -> i ( )
     */
    private DataType functionCall() {
        Lexeme lexeme = scanner.getNext();
        lexemeIsIdent(lexeme);
        DataType dataType = semanticAnalyzer.checkFunctionType(lexeme);
        lexeme = scanner.getNext();
        if (lexeme.getType() != TOpenedParen)
            symbolExpected('(', lexeme);
        lexeme = scanner.getNext();
        if (lexeme.getType() != TClosedParen)
            symbolExpected(')', lexeme);
        return dataType;
    }

    /**
     * Dw -> do B while ( E ) ;
     * E -> O1
     */
    private void doWhileOperator() {
        Lexeme lexeme = scanner.getNext();
        if (lexeme.getType() != KeyDo)
            throw new SyntaxAnalyzerException("Ожидалось ключевое слово do", lexeme);
        block();
        lexeme = scanner.getNext();
        if (lexeme.getType() != KeyWhile)
            throw new SyntaxAnalyzerException("Ожидалось ключевое слово while", lexeme);
        lexeme = scanner.getNext();
        if (lexeme.getType() != TOpenedParen)
            symbolExpected('(', lexeme);
        o1();
        lexeme = scanner.getNext();
        if (lexeme.getType() != TClosedParen)
            symbolExpected(')', lexeme);
        lexeme = scanner.getNext();
        if (lexeme.getType() != TSemicolon)
            symbolExpected(';', lexeme);
    }

    /**
     * R -> return E ;
     * E -> O1
     */
    private void returnOperator() {
        Lexeme lexeme = scanner.getNext();
        if (lexeme.getType() != KeyReturn)
            throw new SyntaxAnalyzerException("Ожидалось ключевое слово return", lexeme);
        DataType returnType = o1();
        lexeme = scanner.getNext();
        Lexeme function = semanticAnalyzer.findParent();
        DataType functionType = semanticAnalyzer.checkFunctionType(function);
        if (!semanticAnalyzer.checkCompatibility(functionType, returnType)) {
            analyzeExceptionBuilder.typeCastingException(functionType, returnType);
        }
        if (lexeme.getType() != TSemicolon)
            symbolExpected(';', lexeme);
        semanticAnalyzer.assignReturn();
    }

    /**
     * Y -> i = E ; | Ae = E ;
     * Ae -> i [ E ]
     * E -> O1
     */
    private void assignOperator() {
        Lexeme lexeme = scanner.getNext();
        lexemeIsIdent(lexeme);
        Lexeme identifier = lexeme;
        DataType variableType = semanticAnalyzer.checkVariableType(lexeme);
        lexeme = scanner.getNext();
        if (lexeme.getType() == TOpenedBracket) {
            DataType expressionType = o1();
            if (!semanticAnalyzer.checkArrayIndex(expressionType)) {
                analyzeExceptionBuilder.unsupportableArrayIndexTypeException(expressionType);
            }
            lexeme = scanner.getNext();
            if (lexeme.getType() != TClosedBracket)
                symbolExpected(')', lexeme);
            if (!semanticAnalyzer.checkArrayType(identifier)) {
                analyzeExceptionBuilder.notArrayException(identifier.getValue());
            }
            lexeme = scanner.getNext();
        }
        if (lexeme.getType() != TAssign)
            symbolExpected('=', lexeme);
        DataType expressionType = o1();
        if (!semanticAnalyzer.checkCompatibility(variableType, expressionType)) {
            analyzeExceptionBuilder.typeCastingException(variableType, expressionType);
        }
        lexeme = scanner.getNext();
        if (lexeme.getType() != TSemicolon)
            symbolExpected(';', lexeme);
    }

    /**
     * O1 -> O1 > O2 | O1 < O2 | O1 <= O2 | O1 >= O2 | O1 == O2 | O1 != O2 | O2
     */
    private DataType o1() {
        DataType dataType = o2();
        while (true) {
            State currentState = scanner.getCurrentState();
            Lexeme lexeme = scanner.getNext();
            if (lexeme.getType() == TLessThen ||
                    lexeme.getType() == TLessThenOrEq ||
                    lexeme.getType() == TGreaterThen ||
                    lexeme.getType() == TGreaterThenOrEq ||
                    lexeme.getType() == TEq ||
                    lexeme.getType() == TIneq) {
                DataType newDataType = o2();
                dataType = semanticAnalyzer.cast(dataType, newDataType);
                continue;
            }
            scanner.restoreState(currentState);
            break;
        }
        return dataType;
    }

    /**
     * O2 -> O2 + O3 | O2 - O3 | O3
     */
    private DataType o2() {
        DataType dataType = o3();
        while (true) {
            State currentState = scanner.getCurrentState();
            Lexeme lexeme = scanner.getNext();
            if (lexeme.getType() == TPlus ||
                    lexeme.getType() == TMinus) {
                DataType newDataType = o3();
                dataType = semanticAnalyzer.cast(dataType, newDataType);
                continue;
            }
            scanner.restoreState(currentState);
            break;
        }
        return dataType;
    }

    /**
     * O3 -> O3 * O4 | O3 / O4 | O3 % O4 | O4
     */
    private DataType o3() {
        DataType dataType = o4();
        while (true) {
            State currentState = scanner.getCurrentState();
            Lexeme lexeme = scanner.getNext();
            if (lexeme.getType() == TMultiple ||
                    lexeme.getType() == TDivision ||
                    lexeme.getType() == TMod) {
                DataType newDataType = o4();
                dataType = semanticAnalyzer.cast(dataType, newDataType);
                continue;
            }
            scanner.restoreState(currentState);
            break;
        }
        return dataType;
    }

    /**
     * O4 -> O4 ++ | O4 -- | O5
     */
    private DataType o4() {
        DataType dataType = o5();
        if (doubleOperation()) {
            //return semanticAnalyzer.cast(DataType.tFloat, dataType);
        }
        return dataType;
    }

    /**
     * O5 -> ++ O5 | -- O5 | O6
     */
    private DataType o5() {
        if (doubleOperation()) {
            //return semanticAnalyzer.cast(DataType.tFloat, o6());
        }
        return o6();
    }

    private boolean doubleOperation() {
        boolean hasOperation = false;
        while (true) {
            State currentState = scanner.getCurrentState();
            Lexeme lexeme = scanner.getNext();
            if (lexeme.getType() == TDoublePlus ||
                    lexeme.getType() == TDoubleMinus) {
                hasOperation = true;
                continue;
            }
            scanner.restoreState(currentState);
            break;
        }
        return hasOperation;
    }

    /**
     * O6 -> i | C | Ae | Fc | ( O1 )
     * Ae -> i [ E ]
     * E -> O1
     * C -> s | c | z10 | z16
     */
    private DataType o6() {
        DataType type = null;
        State currentState = scanner.getCurrentState();
        Lexeme lexeme = scanner.getNext();
        Lexeme identifier = lexeme;
        switch (lexeme.getType()) {
            case TIdent: {
                State state = scanner.getCurrentState();
                lexeme = scanner.getNext();
                if (lexeme.getType() == TOpenedParen) {
                    scanner.restoreState(currentState);
                    type = functionCall();
                    break;
                }
                if (lexeme.getType() == TOpenedBracket) {
                    if (!semanticAnalyzer.checkArrayType(identifier)) {
                        analyzeExceptionBuilder.notArrayException(identifier.getValue());
                    }
                    type = o1();
                    if (!semanticAnalyzer.checkArrayIndex(type)) {
                        analyzeExceptionBuilder.unsupportableArrayIndexTypeException(type);
                    }
                    lexeme = scanner.getNext();
                    type = semanticAnalyzer.checkVariableType(identifier);
                    if (lexeme.getType() != TClosedBracket)
                        symbolExpected(']', lexeme);
                    break;
                }
                type = semanticAnalyzer.checkVariableType(identifier);
                scanner.restoreState(state);
                break;
            }
            case TConstString:
                type = DataType.tChar;
                break;
            case TConstChar:
                type = DataType.tChar;
                break;
            case TConst10:
                type = DataType.tFloat;
                break;
            case TConst16:
                type = DataType.tFloat;
                break;
            case TOpenedParen: {
                type = o1();
                lexeme = scanner.getNext();
                if (lexeme.getType() != TClosedParen)
                    symbolExpected(')', lexeme);
                break;
            }
            default:
                throw new SyntaxAnalyzerException("Ожидалось одно из выражений: " +
                                                          "идентификатор, константная строка, симол, целове число" +
                                                          " в " +
                                                          "10 или 16 с/с, '('", lexeme);
        }
        return type;
    }

    private DataType resolveType(Lexeme lexeme) {
        if (lexeme.getType() == KeyChar) {
            return DataType.tChar;
        }
        if (lexeme.getType() == KeyFloat) {
            return DataType.tFloat;
        }
        throw new SyntaxAnalyzerException("Ожидался тип", lexeme);
    }

    private void lexemeIsIdent(Lexeme lexeme) {
        if (lexeme.getType() != TIdent) {
            throw new SyntaxAnalyzerException("Ожидался идентификатор", lexeme);
        }
    }

    private void symbolExpected(char symbol, Lexeme lexeme) {
        throw new SyntaxAnalyzerException("Ожидался символ '" + symbol + "'", lexeme);
    }

}
