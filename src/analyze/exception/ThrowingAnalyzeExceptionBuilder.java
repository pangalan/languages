package analyze.exception;

import analyze.Scanner;
import enums.DataType;
import exceptions.*;

public class ThrowingAnalyzeExceptionBuilder implements AnalyzeExceptionBuilder {

    private final Scanner scanner;

    public ThrowingAnalyzeExceptionBuilder(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public void typeCastingException(DataType toType, DataType fromType) {
        throw new TypeCastingException(toType, fromType, scanner);
    }

    @Override
    public void unsupportableArrayIndexTypeException(DataType dataType) {
        throw new UnsupportableArrayIndexTypeException(dataType, scanner);
    }

    @Override
    public void notArrayException(String identifier) {
        throw new NotArrayException(identifier, scanner);
    }

    @Override
    public void alreadyDefinedException(String identifier) {
        throw new AlreadyDefinedException(identifier, scanner);
    }

    @Override
    public void unsupportedArraySizeException() {
        throw new UnsupportedArraySizeException(scanner);
    }

    @Override
    public void unknownIdentifierException(String identifier) {
        throw new UnknownIdentifierException(identifier, scanner);
    }

    @Override
    public void notVariableException(String identifier) {
        throw new NotVariableException(identifier, scanner);
    }

    @Override
    public void notFunctionException(String identifier) {
        throw new NotFunctionException(identifier, scanner);
    }

    @Override
    public void notReturnOperator(String identifier) {
        throw new NotReturnOperatorException(identifier, scanner);
    }
}
