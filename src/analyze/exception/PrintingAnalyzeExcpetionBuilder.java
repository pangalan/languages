package analyze.exception;

import analyze.Scanner;
import enums.DataType;
import exceptions.*;

public class PrintingAnalyzeExcpetionBuilder implements AnalyzeExceptionBuilder {

    private final Scanner scanner;
    private static final String MESSAGE = "Ошибка на строке %d на позиции %d:\n\t%s";

    public PrintingAnalyzeExcpetionBuilder(Scanner scanner) {this.scanner = scanner;}

    private void printMessage(String message) {
        System.out.println(String.format(MESSAGE, scanner.getCurrentState().getLine() + 1,
                      scanner.getCurrentState().getPosition() + 1,
                      message));
    }

    @Override
    public void typeCastingException(DataType toType, DataType fromType) {
        printMessage(String.format(TypeCastingException.MESSAGE, fromType, toType));
    }

    @Override
    public void unsupportableArrayIndexTypeException(DataType dataType) {
        printMessage(String.format(UnsupportableArrayIndexTypeException.MESSAGE, dataType));
    }

    @Override
    public void notArrayException(String identifier) {
        printMessage(String.format(NotArrayException.MESSAGE, identifier));
    }

    @Override
    public void alreadyDefinedException(String identifier) {
        printMessage(String.format(AlreadyDefinedException.MESSAGE, identifier));
}

    @Override
    public void unsupportedArraySizeException() {
        printMessage(UnsupportedArraySizeException.MESSAGE);
}

    @Override
    public void unknownIdentifierException(String identifier) {
        printMessage(String.format(UnknownIdentifierException.MESSAGE, identifier));
}

    @Override
    public void notVariableException(String identifier) {
        printMessage(String.format(NotVariableException.MESSAGE, identifier));
    }

    @Override
    public void notFunctionException(String identifier) {
        printMessage(String.format(NotFunctionException.MESSAGE, identifier));
 }

    @Override
    public void notReturnOperator(String identifier) {
        printMessage(String.format(NotReturnOperatorException.MESSAGE, identifier));
    }
}
