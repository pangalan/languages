package analyze.exception;

import enums.DataType;

public interface AnalyzeExceptionBuilder {

    void typeCastingException(DataType toType, DataType fromType);

    void unsupportableArrayIndexTypeException(DataType dataType);

    void notArrayException(String identifier);

    void alreadyDefinedException(String identifier);

    void unsupportedArraySizeException();

    void unknownIdentifierException(String identifier);

    void notVariableException(String identifier);

    void notFunctionException(String identifier);

    void notReturnOperator(String identifier);

}
