package analyze;

import analyze.exception.AnalyzeExceptionBuilder;
import enums.DataType;
import model.Lexeme;

public class SemanticAnalyzer {

    private final Scanner scanner;
    private final AnalyzeExceptionBuilder analyzeExceptionBuilder;

    private final Node root = new Node(null, null, null);
    private Node current = root;

    public SemanticAnalyzer(Scanner scanner, AnalyzeExceptionBuilder analyzeExceptionBuilder) {
        this.scanner = scanner;
        this.analyzeExceptionBuilder = analyzeExceptionBuilder;
    }

    public void addVariable(DataType type, Lexeme lexeme) {
        Node node = findScope(lexeme.getValue());
        if (node != null) {
            addLeft(DataType.tUnknown, lexeme);
            analyzeExceptionBuilder.alreadyDefinedException(lexeme.getValue());
        }
        addLeft(type, lexeme);
    }

    public void addFunction(DataType type, Lexeme lexeme) {
        Node node = findScope(lexeme.getValue());
        if (node != null) {
            analyzeExceptionBuilder.alreadyDefinedException(lexeme.getValue());
        }
        addLeft(type, lexeme);
        addRight();
    }

    public void addBlock() {
        addLeft(DataType.tBlock, null);
        addRight();
    }

    public void addLeft(DataType dataType, Lexeme lexeme) {
        current.left = new Node(current, dataType, lexeme);
        current = current.left;
    }

    public void addRight() {
        current.right = new Node(current, null, null);
        current = current.right;
    }

    public void addArrayLevel(int size) {
        if (current.isArray) {
            analyzeExceptionBuilder.unsupportedArraySizeException();
        }
        current.isArray = true;
        current.arraySize = size;
    }

    public void goToParent() {
        while (current.dataType != null) {
            current = current.parent;
        }
        current = current.parent;
    }

    public DataType checkVariableType(Lexeme lexeme) {
        Node node = find(lexeme.getValue());
        if (node == null) {
            analyzeExceptionBuilder.unknownIdentifierException(lexeme.getValue());
        }
        if (node.right != null) {
            analyzeExceptionBuilder.notVariableException(lexeme.getValue());
        }
        return node.dataType;
    }

    public DataType checkFunctionType(Lexeme lexeme) {
        Node node = find(lexeme.getValue());
        if (node == null) {
            analyzeExceptionBuilder.unknownIdentifierException(lexeme.getValue());
            return DataType.tUnknown;
        }
        if (node.right == null) {
            analyzeExceptionBuilder.notFunctionException(lexeme.getValue());
        }
        return node.dataType;
    }

    public boolean checkArrayIndex(DataType type) {
        return cast(DataType.tChar, type) != null;
    }

    public boolean checkArrayType(Lexeme lexeme) {
        Node node = find(lexeme.getValue());
        if (node == null) {
            analyzeExceptionBuilder.unknownIdentifierException(lexeme.getValue());
        }
        return node.isArray;
    }

    private Node findParentNode() {
        Node node = current;
        while (node.right == null || node.dataType == null || node.dataType == DataType.tBlock) {
            node = node.parent;
        }
        return node;
    }

    public Lexeme findParent() {
        return findParentNode().lexeme;
    }

    public Node find(String identifier) {
        Node node = current;
        do {
            if (node.lexeme != null && node.lexeme.getValue().equals(identifier)){
                return node;
            }
            node = node.parent;
        } while (node != null);
        return null;
    }

    public void assignReturn() {
        findParentNode().hasReturn = true;
    }

    public boolean checkReturn() {
        return findParentNode().hasReturn;
    }

    public boolean checkCompatibility(DataType toType, DataType fromType) {
        DataType type = cast(toType, fromType);
        return type != DataType.tUnknown;
    }

    public DataType cast(DataType toType, DataType fromType) {
        if (toType == fromType) {
            return toType;
        }
        if (toType == DataType.tFloat && fromType == DataType.tChar) {
            return DataType.tFloat;
        }
        return DataType.tUnknown;
    }

    public Node findScope(String identifier) {
        Node node = current;
        while (node.lexeme != null) {
            if (node.lexeme.getValue().equals(identifier)) {
                return node;
            }
            node = node.parent;
        }
        return null;
    }

    public String treeToString() {
        StringBuilder builder = new StringBuilder();
        printTree(builder, root, 0);
        return builder.toString();
    }

    private void printTree(StringBuilder builder, Node node, int level) {
        if(node == null) return;

        for(int i = 0; i < level; i++) {
            builder.append("|");
            for(int j = 0; j < 6; j++) builder.append(" ");
        }

        String value = node.lexeme == null ? "" : node.lexeme.getValue();
        String array = node.isArray ? "[" + node.arraySize + "]" : "";
        builder.append(node.dataType == null ? "♦" : (node.dataType + " " + value + array));
        if(node == current) builder.append("  <--");

        builder.append("\n");
        printTree(builder, node.right, level + 1);
        printTree(builder, node.left, level);
    }

    private static class Node {
        public final Node parent;
        public Node left;
        public Node right;

        public Lexeme lexeme;
        public DataType dataType;
        public Integer arraySize;
        public boolean isArray = false;
        public boolean hasReturn = false;

        public Node(Node parent, DataType dataType, Lexeme lexeme) {
            this.parent = parent;
            this.lexeme = lexeme;
            this.dataType = dataType;
        }
    }

}
