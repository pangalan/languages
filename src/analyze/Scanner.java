package analyze;

import enums.Type;
import model.Lexeme;
import model.State;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Scanner {

    private static final int LEXEME_MAX_LENGTH = 10;
    private static final Map<String, Type> keywords = new HashMap<>();

    static {
        keywords.put("do", Type.KeyDo);
        keywords.put("while", Type.KeyWhile);
        keywords.put("const", Type.KeyConst);
        keywords.put("float", Type.KeyFloat);
        keywords.put("char", Type.KeyChar);
        keywords.put("return", Type.KeyReturn);
    }

    private List<char[]> source;
    private int ptr;
    private int line;
    private int startPtr;
    private int startLine;
    private StringBuilder value;
    private char[] currentLine;

    public Scanner(List<String> text) {
        source = text.stream().map(String::toCharArray).collect(Collectors.toList());
        ptr = line = 0;
        if (!source.isEmpty())
            currentLine = source.get(0);
    }

    public Lexeme getNextAndRestoreState() {
        State currentState = getCurrentState();
        Lexeme lexeme = getNext();
        restoreState(currentState);
        return lexeme;
    }

    public State getCurrentState() {
        return new State(line, ptr);
    }

    public void restoreState(State state) {
        line = state.getLine();
        ptr = state.getPosition();
        currentLine = source.get(line);
    }

    public Lexeme getNext() {
        try {
            return next();
        } catch (Exception e) {
            return getLexeme(Type.TError);
        }
    }

    private Lexeme next() {
        skipCharacters();
        startLine = line;
        startPtr = ptr;
        value = new StringBuilder();
        if (currentLine == null || currentLine[ptr] == '\0') {
            return getLexeme(Type.TEnd);
        }
        if (isLetter()) {
            do {
                value.append(currentLine[ptr++]);
            } while (isLetter() || isNumber() || currentLine[ptr] == '_');
            Type type = keywords.get(value.toString());
            return getLexeme(type == null ? Type.TIdent : type);
        }
        if (isNumber()) {
            if (currentLine[ptr] == '0' && (currentLine[ptr + 1] == 'x') || (currentLine[ptr + 1] == 'X')) {
                value.append(currentLine[ptr]).append(currentLine[ptr + 1]);
                ptr += 2;
                do {
                    value.append(currentLine[ptr++]);
                } while (isNumber() || isLetter16());
                return getLexeme(Type.TConst16);
            }
            do {
                value.append(currentLine[ptr++]);
            } while (isNumber());
            return getLexeme(Type.TConst10);
        }
        if (currentLine[ptr] == '"') {
            return getConstString();
        }
        if (currentLine[ptr] == '\'') {
            ptr++;
            if (currentLine[ptr] == '\'') {
                return getLexeme(Type.TError);
            }
            value.append(currentLine[ptr++]);
            if (currentLine[ptr] != '\'') {
                return getLexeme(Type.TError);
            }
            ptr++;
            return getLexeme(Type.TConstChar);
        }
        ptr++;
        switch (currentLine[ptr - 1]) {
            case '<': {
                if (currentLine[ptr] == '=') {
                    ptr++;
                    return getLexeme(Type.TLessThenOrEq);
                }
                return getLexeme(Type.TLessThen);
            }
            case '>': {
                if (currentLine[ptr] == '=') {
                    ptr++;
                    return getLexeme(Type.TGreaterThenOrEq);
                }
                return getLexeme(Type.TGreaterThen);
            }
            case '=': {
                if (currentLine[ptr] == '=') {
                    ptr++;
                    return getLexeme(Type.TEq);
                }
                return getLexeme(Type.TAssign);
            }
            case '!': {
                if (currentLine[ptr] == '=') {
                    ptr++;
                    return getLexeme(Type.TIneq);
                }
                return getLexeme(Type.TError);
            }
            case '+': {
                if (currentLine[ptr] == '+') {
                    ptr++;
                    return getLexeme(Type.TDoublePlus);
                }
                return getLexeme(Type.TPlus);
            }
            case '-': {
                if (currentLine[ptr] == '-') {
                    ptr++;
                    return getLexeme(Type.TDoubleMinus);
                }
                return getLexeme(Type.TMinus);
            }
            case '*':
                return getLexeme(Type.TMultiple);
            case '/':
                return getLexeme(Type.TDivision);
            case '%':
                return getLexeme(Type.TMod);
            case ',':
                return getLexeme(Type.TComma);
            case ';':
                return getLexeme(Type.TSemicolon);
            case '(':
                return getLexeme(Type.TOpenedParen);
            case ')':
                return getLexeme(Type.TClosedParen);
            case '{':
                return getLexeme(Type.TOpenedBrace);
            case '}':
                return getLexeme(Type.TClosedBrace);
            case '[':
                return getLexeme(Type.TOpenedBracket);
            case ']':
                return getLexeme(Type.TClosedBracket);
        }
        return getLexeme(Type.TError);
    }

    private Lexeme getConstString() {
        boolean isTooLong = false;
        ptr++;
        while (currentLine[ptr] != '"') {
            if (currentLine[ptr] == '\n') {
                nextLine();
            }
            if (value.length() < LEXEME_MAX_LENGTH) {
                value.append(currentLine[ptr]);
            } else {
                isTooLong = true;
            }
            ptr++;
        }
        ptr++;
        if (isTooLong) {
            System.out.println("Лексема превышает допустимую длину и будет обрезана");
        }
        return getLexeme(Type.TConstString);
    }

    private boolean isLetter16() {
        return (currentLine[ptr] >= 'a' && currentLine[ptr] <= 'f') ||
                (currentLine[ptr] >= 'A' && currentLine[ptr] <= 'F');
    }

    private boolean isNumber() {
        return (currentLine[ptr] >= '0' && currentLine[ptr] <= '9');
    }

    private boolean isLetter() {
        return (currentLine[ptr] >= 'a' && currentLine[ptr] <= 'z') ||
                (currentLine[ptr] >= 'A' && currentLine[ptr] <= 'Z');
    }

    private Lexeme getLexeme(Type type) {
        boolean isError = type == Type.TError;
        return new Lexeme(type, value.toString(), isError ? line : startLine, isError ? ptr : startPtr);
    }

    private void skipCharacters() {
        while (true) {
            if (currentLine == null)
                break;
            if (currentLine[ptr] == ' ' || currentLine[ptr] == '\t') {
                ptr++;
            } else if (isEndOfLine()) {
                nextLine();
            } else if (isSingleComment()) {
                nextLine();
            } else if (isMultilineComment()) {
                while (!isEndOfMultilineComment() && currentLine != null) {
                    ptr++;
                    while (isEndOfLine()) {
                        nextLine();
                    }
                }
                ptr += 2;
            } else break;
        }
    }

    private boolean isEndOfLine() {
        return currentLine != null && currentLine[ptr] == '\n';
    }

    private boolean isEndOfMultilineComment() {
        if (currentLine != null && ptr + 1 < currentLine.length) {
            return currentLine[ptr] == '*' && currentLine[ptr + 1] == '/';
        }
        return false;
    }

    private boolean isMultilineComment() {
        if (ptr + 1 < currentLine.length) {
            return currentLine[ptr] == '/' && currentLine[ptr + 1] == '*';
        }
        return false;
    }

    private boolean isSingleComment() {
        if (ptr + 1 < currentLine.length) {
            return currentLine[ptr] == '/' && currentLine[ptr + 1] == '/';
        }
        return false;
    }

    private void nextLine() {
        line++;
        ptr = 0;
        currentLine = line < source.size() ? source.get(line) : null;
    }
}
