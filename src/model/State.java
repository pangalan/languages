package model;

public class State {

    private int line;
    private int position;

    public State(int line, int position) {
        this.line = line;
        this.position = position;
    }

    public int getLine() {
        return line;
    }

    public int getPosition() {
        return position;
    }
}
