package model;

import enums.Type;

import java.util.Optional;

public class Lexeme {

    private Type type;
    private String value;
    private int line;
    private int position;

    public Lexeme(Type type, String value, int line, int position) {
        this.type = type;
        this.value = value;
        this.line = line + 1;
        this.position = position + 1;
    }

    @Override
    public String toString() {
        if (type == Type.TError) {
            return "Ошибка на строке " + line + " на позиции " + position + "\t:\t" + value;
        }
        return type + " : " + Optional.ofNullable(type.getDisplayName()).orElse(value);
    }

    public Type getType() {
        return type;
    }

    public int getLine() {
        return line;
    }

    public int getPosition() {
        return position;
    }

    public String getValue() {
        return value;
    }
}
